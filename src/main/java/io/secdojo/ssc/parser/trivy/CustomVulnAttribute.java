package io.secdojo.ssc.parser.trivy;

public enum CustomVulnAttribute implements com.fortify.plugin.spi.VulnerabilityAttribute {

    // Custom attributes must have their types defined:
    InstalledVersion(AttrType.STRING), //
    FixedVersion(AttrType.STRING), //
    PrimaryURL(AttrType.STRING), //
    Title(AttrType.STRING), //
    Description(AttrType.LONG_STRING), //
    References(AttrType.LONG_STRING), //
    PublishedDate(AttrType.STRING), //
    LastModifiedDate(AttrType.STRING), //
    nvdV2Vecor(AttrType.STRING), //
    nvdV3Vector(AttrType.STRING), //
    nvdV2Score(AttrType.STRING), //
    nvdV3Score(AttrType.STRING), //
    redhatV3Vector(AttrType.STRING), //
    redhatV3Score(AttrType.STRING), //
    Cwe(AttrType.STRING),
    ;

	private final AttrType attributeType;

    CustomVulnAttribute(final AttrType attributeType) {
        this.attributeType = attributeType;
    }

    @Override
    public String attributeName() {
        return name();
    }

    @Override
    public AttrType attributeType() {
        return attributeType;
    }
}
