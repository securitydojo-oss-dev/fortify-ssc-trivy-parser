package io.secdojo.ssc.parser.trivy.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Result {

    @JsonProperty("Target")
    private String target;
    @JsonProperty("Type")
    private String type;
    @JsonProperty("Vulnerabilities")
    private Vulnerability[] vulnerabilities;
}
