package io.secdojo.ssc.parser.trivy.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import io.secdojo.ssc.parser.trivy.domain.*;

@Getter
public class CVSS {
    @JsonProperty("nvd")
    private Nvd nvd;
    @JsonProperty("redhat")
    private Redhat redhat;
}
