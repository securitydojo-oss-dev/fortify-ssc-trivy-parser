package io.secdojo.ssc.parser.trivy.parser;

import java.io.*;
import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.secdojo.ssc.parser.trivy.domain.*;

import com.fasterxml.jackson.core.JsonToken;
import com.fortify.plugin.api.BasicVulnerabilityBuilder.Priority;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.StaticVulnerabilityBuilder;
import com.fortify.plugin.api.VulnerabilityHandler;
import io.secdojo.ssc.parser.trivy.CustomVulnAttribute;
import com.fortify.util.ssc.parser.EngineTypeHelper;
import com.fortify.util.ssc.parser.json.ScanDataStreamingJsonParser;
import io.secdojo.ssc.parser.trivy.domain.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VulnerabilitiesParser {
    private static final Logger LOG = LoggerFactory.getLogger(VulnerabilitiesParser.class);

    private static final String ENGINE_TYPE = EngineTypeHelper.getEngineType();

    private static final Map<String, Priority> MAP_SEVERITY_TO_PRIORITY = Stream.of(
            new AbstractMap.SimpleImmutableEntry<>("LOW", Priority.Low),
            new AbstractMap.SimpleImmutableEntry<>("MEDIUM", Priority.Medium),
            new AbstractMap.SimpleImmutableEntry<>("HIGH", Priority.High),
            new AbstractMap.SimpleImmutableEntry<>("CRITICAL", Priority.Critical))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    private final ScanData scanData;
    private final VulnerabilityHandler vulnerabilityHandler;


    public VulnerabilitiesParser(final ScanData scanData, final VulnerabilityHandler vulnerabilityHandler) {
        this.scanData = scanData;
        this.vulnerabilityHandler = vulnerabilityHandler;
    }


    /**
     * Main method to commence parsing the input provided by the configured {@link ScanData}.
     *
     * @throws IOException
     */
    public final void parse() throws IOException {
        LOG.info("VulnerabilitiesParser parse starting");
        new ScanDataStreamingJsonParser()
                .expectedStartTokens(JsonToken.START_ARRAY)
                .handler("/Vulnerabilities/*", Vulnerability.class, this::buildVulnerability)
                .parse(scanData);
    }


    private void handleResult(Result result) {
        Vulnerability[] vulnerabilities = result.getVulnerabilities();
        if(vulnerabilities != null) {
            for (Vulnerability vulnerability : vulnerabilities) {
                buildVulnerability(vulnerability);
            }
        }
    }


    /**
     * Build the vulnerability from the given {@link Vulnerability}, using the configured {@link VulnerabilityHandler}.
     *
     * @param vuln
     */
    private void buildVulnerability(Vulnerability vuln) {
        LOG.info("VulnerabilitiesParser buildVulnerability");
        StaticVulnerabilityBuilder vb = vulnerabilityHandler.startStaticVulnerability(vuln.getVulnerabilityID());

        // Set meta-data
        vb.setEngineType(ENGINE_TYPE);
        vb.setAnalyzer("Trivy");

        // Set mandatory values to JavaDoc-recommended values
        vb.setAccuracy(5.0f);
        vb.setConfidence(2.5f);


        // Set standard vulnerability fields based on input
        vb.setFileName(vuln.getPckName());
        vb.setPriority(this.getPriority(vuln));
        vb.setCategory(vuln.getVulnerabilityID());
        // Set custom attributes based on input
        vb.setStringCustomAttributeValue(CustomVulnAttribute.Description, vuln.getDescription());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.PublishedDate, vuln.getPublishedDate());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.LastModifiedDate, vuln.getLastModifiedDate());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.InstalledVersion, vuln.getInstalledVersion());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.FixedVersion, vuln.getFixedVersion());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.Title, StringUtils.defaultIfEmpty(vuln.getTitle(), vuln.getPckName()));
        vb.setStringCustomAttributeValue(CustomVulnAttribute.PrimaryURL, vuln.getPrimaryUrl());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.References, this.getReferences(vuln.getReferences()));
        vb.setStringCustomAttributeValue(CustomVulnAttribute.Cwe, this.getCwe(vuln.getCweIDs()));

        CVSS cvss= vuln.getCvss();
        if(cvss != null) {
            Nvd nvd = cvss.getNvd();
            Redhat redhat = cvss.getRedhat();
            if(nvd != null) {
                vb.setStringCustomAttributeValue(CustomVulnAttribute.nvdV2Vecor, nvd.getV2Vector());
                vb.setStringCustomAttributeValue(CustomVulnAttribute.nvdV2Score, nvd.getV2Score());
                vb.setStringCustomAttributeValue(CustomVulnAttribute.nvdV3Score,nvd.getV3Score());
                vb.setStringCustomAttributeValue(CustomVulnAttribute.nvdV3Vector, nvd.getV3Vector());
            }
            if(redhat !=  null) {
                vb.setStringCustomAttributeValue(CustomVulnAttribute.redhatV3Score, redhat.getV3Score());
                vb.setStringCustomAttributeValue(CustomVulnAttribute.redhatV3Vector, redhat.getV3Score());
            }
        }


        vb.completeVulnerability();
    }

    private String getCwe(String[] cweIDs) {
        StringBuilder sb = new StringBuilder();
        if(cweIDs != null) {
            for (String cwe : cweIDs) {
                sb.append(cwe).append(" ");
            }
        }
        return sb.toString();
    }

    private String getReferences(String[] references) {
        StringBuilder sb = new StringBuilder();
        if(references != null) {
            for (String reference : references) {
                sb.append(reference).append("<br/>\n");
            }
        }
        return sb.toString();
    }

    private Priority getPriority(Vulnerability vuln) {
        return MAP_SEVERITY_TO_PRIORITY.getOrDefault(vuln.getSeverity(), Priority.Medium);
    }
}