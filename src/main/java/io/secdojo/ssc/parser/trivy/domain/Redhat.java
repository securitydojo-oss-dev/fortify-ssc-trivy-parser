package io.secdojo.ssc.parser.trivy.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Redhat {
    @JsonProperty("V3Vector")
    private String V3Vector;
    @JsonProperty("V3Score")
    private String V3Score;
}
