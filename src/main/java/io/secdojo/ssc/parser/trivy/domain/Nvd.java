package io.secdojo.ssc.parser.trivy.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Nvd {
    @JsonProperty("V2Vector")
    private String V2Vector;
    @JsonProperty("V3Vector")
    private String V3Vector;
    @JsonProperty("V2Score")
    private String V2Score;
    @JsonProperty("V3Score")
    private String V3Score;
}
